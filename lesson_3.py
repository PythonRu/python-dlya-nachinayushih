'''
Задания к уроку 3. Типы данных
'''

# 1. Присвойте переменной целое число и получите вывод <class 'int'>
current_month =
type_var = type(current_month)
print(type_var)
# требуемый вывод:
# <class 'int'>

# 2. Исправьте ошибки в коде, что бы получить требуемый вывод.
polar_radius_title = "Радиус" "Земли"
polar_radius_float = 6378,1 
print(polar_radius_title, polar_radius_float)
# требуемый вывод:
# Радиус Земли 6378.1

# 3. Создайте переменные, что бы вывести требуемый текст
str_type, int_type, float_type, bool_type = 
print("Типы данных: ", end="")
print(type(str_type), type(int_type, type(float_type), type(bool_type), sep=",")
# требуемый вывод:
# Типы данных: <class 'str'>, <class 'int'>, <class 'float'>, <class 'bool'>
